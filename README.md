# xNuke

is a Portal/CMS system based on PHP-Nuke. Since it seams to have halted on further development, I have decided to attempt modernizing that.
.

Read the [Readme](Readmen.txt) for further information about [PHP-Nuke](https://bitbucket.org/phpnuke/phpnuke).


## License(s)
[***PHP-Nuke***](https://bitbucket.org/phpnuke/phpnuke) is licensed under the [*GPLv2*](Copying.txt).

[**xNuke**](https://gitlab.com/hyperclock/xnuke) will be upgraded to [*GPLv3*](LICENSE).
